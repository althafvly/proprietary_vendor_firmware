LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),raven)
$(call add-radio-file,radio/bl1.img)
$(call add-radio-file,radio/pbl.img)
$(call add-radio-file,radio/bl2.img)
$(call add-radio-file,radio/abl.img)
$(call add-radio-file,radio/bl31.img)
$(call add-radio-file,radio/tzsw.img)
$(call add-radio-file,radio/gsa.img)
$(call add-radio-file,radio/ldfw.img)
$(call add-radio-file,radio/modem.img)
endif
