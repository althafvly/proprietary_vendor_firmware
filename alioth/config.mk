AB_OTA_PARTITIONS += \
    abl \
    aop \
    bluetooth \
    cmnlib64 \
    cmnlib \
    devcfg \
    dsp \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    modem \
    qupfw \
    tz \
    uefisecapp \
    xbl_config \
    xbl
